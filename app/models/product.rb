class Product < ActiveRecord::Base

  has_many :reports, through: :orders

  validates :price, :numericality => { :only_integer => true }
  validates :quantity, :numericality => { :greater_than_or_equal_to => 0}

  def name_with_quantity
    "#{name} Product | #{quantity} Product"
  end
end
