class Order < ActiveRecord::Base
  has_many :products
  has_many :reports, through: :products


  validates :quantity, :numericality => { :only_integer => true }

end
