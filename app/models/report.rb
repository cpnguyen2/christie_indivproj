class Report < ActiveRecord::Base
  belongs_to :product
  belongs_to :order

  def products
    @products ||= find_products
  end

  def orders
    @orders ||= find_orders
    @orders ||= find_average
  end


  def find_products
    products = Product.order(:name)
    products = products.where("quantity <= ?",quantity)
  end

  def find_order
    order = Order.order(:sale_date)
    order = orders.where("order_date >= ?", start_date)
    order = orders.where("order_date <= ?", end_date)
  end

  def find_average
    orders = Order.order(:quantity)
    orders = orders.where("product_id == ?", "".average("quantity"))
  end
end

