json.array!(@orders) do |order|
  json.extract! order, :id, :product_id, :quantity, :price, :total, :date
  json.url order_url(order, format: :json)
end
