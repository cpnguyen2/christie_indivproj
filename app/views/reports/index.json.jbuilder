json.array!(@reports) do |report|
  json.extract! report, :id, :product_id, :order_id, :quantity, :name, :start_date, :end_date
  json.url report_url(report, format: :json)
end
