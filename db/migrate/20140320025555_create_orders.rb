class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :product_id
      t.integer :quantity
      t.float :price
      t.integer :total
      t.date :date

      t.timestamps
    end
  end
end
