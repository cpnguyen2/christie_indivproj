class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.integer :product_id
      t.integer :order_id
      t.integer :quantity
      t.string :name
      t.date :start_date
      t.date :end_date

      t.timestamps
    end
  end
end
